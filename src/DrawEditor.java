/**
 * ドローエディタのエントリポイント クラス
 *
 */
public class DrawEditor {
    /**
     * エントリポイント このメソッドが最初に呼ばれる
     * <p/>
     * メインフレームであるDrawFrameのインスタンスを作成する
     *
     * @param args コマンドライン引数 今回は使用されない
     */
    public static void main(String args[]) {
        new DrawFrame();
    }
}
