import javax.swing.*;

/**
 * 各種設定の変更に使うメニューバーを提供する
 *
 */
public class ConfigPanel extends JMenuBar {

    /**
     * コンストラクタ
     * <p/>
     * 各種設定については別のViewで行なっているため、ここではメニューバーにアイテムを追加する処理のみ行っている
     *
     * @param m 各種メニュー項目のViewに渡すDrawModel
     */
    public ConfigPanel(DrawModel m) {
        FileMenuView fileMenu = new FileMenuView(m);
        EditMenuView editMenu = new EditMenuView(m);
        ColorMenuView colorMenu = new ColorMenuView(m);
        ShapeMenuView shapeMenu = new ShapeMenuView(m);
        add(fileMenu);
        add(editMenu);
        add(colorMenu);
        add(shapeMenu);
    }
}
