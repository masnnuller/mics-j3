import javax.swing.*;

/**
 * 図形選択のMenuを提供するView,発生したイベントはShapeMenuControllerに送られる
 */
public class ShapeMenuView extends JMenu {
    private DrawModel model;
    private DrawController controller;
    private ShapeMenuController menuController;

    /**
     * コンストラクタ 各種図形の選択を行うラジオボタンを作成し、ShapeMenuControllerをActionListenerとして登録する
     *
     * @param m ShapeMenuControllerに渡されるDrawModel
     */
    public ShapeMenuView(DrawModel m) {
        super("Shape");
        model = m;
        menuController = new ShapeMenuController(m);
        ButtonGroup g = new ButtonGroup();
        JRadioButtonMenuItem line = new JRadioButtonMenuItem("Line");
        JRadioButtonMenuItem rect = new JRadioButtonMenuItem("Rectangle");
        JRadioButtonMenuItem oval = new JRadioButtonMenuItem("Oval");
        line.setSelected(true);
        line.addActionListener(menuController);
        rect.addActionListener(menuController);
        oval.addActionListener(menuController);
        g.add(line);
        g.add(rect);
        g.add(oval);
        add(line);
        add(rect);
        add(oval);
    }
}
