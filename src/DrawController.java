import java.awt.event.*;

/**
 * Created by masn19 on 15/06/09.
 */
public class DrawController implements MouseListener, MouseMotionListener, KeyListener {
    protected DrawModel model;
    protected int dragStartX, dragStartY;

    public DrawController(DrawModel a) {
        model = a;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        dragStartX = mouseEvent.getX();
        dragStartY = mouseEvent.getY();
        model.createFigure(dragStartX, dragStartY);
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        model.reshapeFigure(dragStartX, dragStartY, mouseEvent.getX(), mouseEvent.getY());
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        model.reshapeFigure(dragStartX, dragStartY, mouseEvent.getX(), mouseEvent.getY());
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_Z:
                model.undo();
                break;
            case KeyEvent.VK_R:
                model.redo();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
