import java.awt.*;

/**
 * 直線を描画するクラス
 */
public class LineFigure extends Figure {
    public LineFigure(int x, int y, int w, int h, Color c, boolean fill) {
        super(x, y, w, h, c);
        width = x;
        height = y;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        g.drawLine(x, y, width, height);
    }

    @Override
    public void reshape(int x1, int y1, int x2, int y2) {
        super.setLocation(x1, y1);
        super.setSize(x2, y2);
    }
}
