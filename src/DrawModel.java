import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Observable;

/**
 * 描画を処理するモデル
 */
public class DrawModel extends Observable {
    protected ArrayList<Figure> fig;
    protected String currentFigure;
    protected Figure drawingFigure;
    protected Color currentColor;
    protected ArrayList<Figure> mem;
    private boolean fill;
    private boolean cleared;

    /**
     * コンストラクタ
     */
    public DrawModel() {
        fig = new ArrayList<Figure>();
        mem = new ArrayList<Figure>();
        drawingFigure = null;
        currentColor = Color.red;
        currentFigure = "None";
        fill = false;
    }

    /**
     * 描画色の変更
     *
     * @param c 変更するColorオブジェクト
     */
    public void setCurrentColor(Color c) {
        currentColor = c;
    }

    /**
     * 現在登録されているFigureのリストを返す
     *
     * @return 現在登録されている(描画するべき)Figureのリスト
     */
    public ArrayList<Figure> getFigures() {
        return fig;
    }

    /**
     * 現在登録されているFigureを全消去する
     */
    public void clearFigure() {
        fig.clear();
        setChanged();
        notifyObservers();
    }

    /**
     * 新しい図形を登録する
     * @param x 初期X座標
     * @param y 初期Y座標
     */
    public void createFigure(int x, int y) {
        Figure f = getCurrentFigure(x, y);
        fig.add(f);
        drawingFigure = f;
        setChanged();
        notifyObservers();
    }

    /**
     * 図形の座標、サイズを更新する
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public void reshapeFigure(int x1, int y1, int x2, int y2) {
        mem.clear();
        if (drawingFigure != null) {
            drawingFigure.reshape(x1, y1, x2, y2);
        }
        setChanged();
        notifyObservers();
    }

    /**
     * Undoの取り消し
     */
    public void redo() {
        if (mem.size() != 0) {
            fig.add(mem.get(mem.size() - 1));
            mem.remove(mem.size() - 1);
            setChanged();
            notifyObservers();
        }
    }

    /**
     * 操作のやりなおし
     */
    public void undo() {
        if (fig.size() != 0) {
            mem.add(fig.get(fig.size() - 1));
            fig.remove(fig.size() - 1);
            setChanged();
            notifyObservers();
        }
    }

    /**
     * 設定に基づいて新しいFigureインスタンスを作成する
     *
     * @param x 初期X座標
     * @param y 初期Y座標
     * @return 作成されたFigureインスタンス
     */
    public Figure getCurrentFigure(int x, int y) {
        switch (currentFigure) {
            case "Rectangle":
                return new RectangleFigure(x, y, 0, 0, currentColor, fill);
            case "Line":
                return new LineFigure(x, y, x, y, currentColor, fill);
            case "Oval":
                return new OvalFigure(x, y, 0, 0, currentColor, fill);
            default:
                return new LineFigure(x, y, x, y, currentColor, fill);
        }
    }

    /**
     * getCurrentFigureで作成されるインスタンスの種類を変更する
     *
     * @param newFigure 描画を行う図形の種類
     */
    public void setCurrentFigure(String newFigure) {
        currentFigure = newFigure;
    }

    /**
     * 塗りつぶしを行うかを設定する
     *
     * @param b
     */
    public void setFill(boolean b) {
        fill = b;
    }

    /**
     * 現在の状態を独自形式ファイルに保存する
     *
     * @param f
     */
    public void saveObjects(File f) {
        try {
            ObjectOutput out = new ObjectOutputStream(new FileOutputStream(f));
            out.writeObject(fig);
            out.flush();
            out.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * 独自形式ファイルをロードし、状態を復元する
     *
     * @param f
     */
    public void loadObjects(File f) {
        try {
            ObjectInput in = new ObjectInputStream(new FileInputStream(f));
            fig = (ArrayList<Figure>) in.readObject();
            in.close();
            setChanged();
            notifyObservers();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * 現在の描画領域をpngイメージに書き出す
     *
     * @param f
     * @param root ルートペイン
     */
    public void exportImage(File f, JRootPane root) {
        BufferedImage image = new BufferedImage(root.getWidth(), root.getHeight(), BufferedImage.TYPE_INT_BGR);
        Graphics2D g = image.createGraphics();
        g.setBackground(Color.WHITE);
        g.clearRect(0, 0, root.getWidth(), root.getHeight());
        for (Figure figure : fig) {
            figure.draw(g);
        }
        g.drawImage(image, 0, 0, root);
        try {
            ImageIO.write(image, "png", f);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
