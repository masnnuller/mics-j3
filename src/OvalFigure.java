import java.awt.*;

/**
 * 円(楕円)を描画するクラス
 */
public class OvalFigure extends Figure {
    private boolean fill;

    public OvalFigure(int x, int y, int w, int h, Color c, boolean _fill) {
        super(x, y, w, h, c);
        fill = _fill;
    }

    /**
     * 円を描画する
     *
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        if (fill) {
            g.fillOval(x, y, width, height);
        } else {
            g.drawOval(x, y, width, height);
        }
    }
}
