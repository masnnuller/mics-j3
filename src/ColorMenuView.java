import javax.swing.*;

/**
 * 色や塗り潰しのメニューを提供するView
 *
 * イベント処理はColorMenuControllerが行う
 */
public class ColorMenuView extends JMenu {
    /**
     * コンストラクタ
     * @param m
     */
    public ColorMenuView(DrawModel m) {
        super("Color");
        ColorMenuController controller = new ColorMenuController(m);

        JMenuItem choose = new JMenuItem("Choose Color");
        JMenuItem reset = new JMenuItem("Reset Color");
        ButtonGroup fillOrStroke = new ButtonGroup();
        JRadioButtonMenuItem fill = new JRadioButtonMenuItem("Fill");
        JMenuItem stroke = new JRadioButtonMenuItem("Stroke");
        stroke.setSelected(true);
        fillOrStroke.add(fill);
        fillOrStroke.add(stroke);

        choose.addActionListener(controller);
        reset.addActionListener(controller);
        fill.addActionListener(controller);
        stroke.addActionListener(controller);

        add(choose);
        add(reset);
        add(stroke);
        add(fill);
    }
}
