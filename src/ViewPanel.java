import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * 描画領域を提供するView, Observerを継承することによりDrawModelからのイベントを受けとり、
 * Figureクラスのdrawを呼びだすことで描画を行う。
 *
 */
public class ViewPanel extends JPanel implements Observer {
    protected DrawModel model;

    /**
     * コンストラクタ
     *
     * @param m 描画を管理するモデル
     * @param c この描画領域で起きたマウス、キーボードからのイベントを処理するコントローラ
     */
    public ViewPanel(DrawModel m, DrawController c) {
        this.setBackground(Color.WHITE);
        this.addMouseListener(c);
        this.addMouseMotionListener(c);
        this.addKeyListener(c);
        setFocusable(true);
        requestFocusInWindow();
        model = m;
        model.addObserver(this);
    }

    /**
     * コンポーネントを描画する
     *
     * モデルから各図形オブジェクトを取得、描画を行う
     * JComponentのメソッドのオーバーライド
     *
     * @param g 使用しない
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        ArrayList<Figure> fig = model.getFigures();
        for (Figure f : fig) {
            f.draw(g);
        }
    }

    /**
     * ObservableであるDrawModelからの通知に応じて再描画を行う
     *
     * paintComponentメソッドも呼び出されるため、図形の更新が反映される
     *
     * @param observable 使用しない
     * @param o 使用しない
     */
    @Override
    public void update(Observable observable, Object o) {
        repaint();
    }
}
