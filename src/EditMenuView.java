import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 編集メニューを提供するView
 *
 * コントローラは内部で実装
 */
public class EditMenuView extends JMenu {
    private DrawModel model;

    /**
     * コンストラクタ
     *
     * @param m 操作対象のDrawModel
     */
    public EditMenuView(DrawModel m) {
        super("Edit");
        model = m;
        JMenuItem undo = new JMenuItem("Undo");
        JMenuItem redo = new JMenuItem("Redo");
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getActionCommand().startsWith("Undo"))
                    model.undo();
                if (actionEvent.getActionCommand().startsWith("Redo"))
                    model.redo();
            }
        };
        undo.addActionListener(listener);
        redo.addActionListener(listener);
        add(undo);
        add(redo);
    }
}
