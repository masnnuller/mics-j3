import java.awt.*;

/**
 * 長方形を描画するクラス
 */
public class RectangleFigure extends Figure {
    private boolean fill;

    public RectangleFigure(int x, int y, int w, int h, Color c, boolean _fill) {
        super(x, y, w, h, c);
        fill = _fill;
    }

    /**
     * 長方形の描画
     *
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        if (fill) {
            g.fillRect(x, y, width, height);
        } else {
            g.drawRect(x, y, width, height);
        }
    }
}
