import java.awt.*;
import java.io.Serializable;

/**
 * 図形を表現するクラス
 *
 * このクラスを継承して各図形を描画するクラスを定義する
 */
public class Figure implements Serializable {
    protected int x, y;
    protected int width, height;
    protected Color color;

    /**
     * コンストラクタ
     *
     * @param x 始点のX座標
     * @param y 始点のY座標
     * @param w 開始時の幅
     * @param h 開始時の高さ
     * @param c 描画色
     */
    public Figure(int x, int y, int w, int h, Color c) {
        this.x = x;
        this.y = y;
        this.color = c;
    }

    /**
     * サイズの変更
     *
     * @param w 幅
     * @param h 高さ
     */
    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    /**
     * 位置の変更
     *
     * @param x X座標
     * @param y Y座標
     */
    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * 図形の描画
     *
     * Figureでは実装をもたない
     *
     * @param g
     */
    public void draw(Graphics g) {
    }

    /**
     * 形状の変更
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public void reshape(int x1, int y1, int x2, int y2) {
        setLocation(Math.min(x1, x2), Math.min(y1, y2));
        setSize(Math.abs(x1 - x2), Math.abs(y1 - y2));
    }
}

