import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ColorMenuViewからのイベントを処理するコントローラ
 */
public class ColorMenuController implements ChangeListener, ActionListener {
    private DrawModel model;
    private JDialog dialog;
    private JColorChooser chooser;

    /**
     * コンストラクタ
     *
     * @param m 処理の対象になるDrawModel
     */
    public ColorMenuController(DrawModel m) {
        model = m;
        dialog = new JDialog();
        dialog.setTitle("Select Color");
        chooser = new JColorChooser(Color.RED);
        chooser.getSelectionModel().addChangeListener(this);
        dialog.getContentPane().add(chooser);
        dialog.setSize(400, 400);
        dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
    }

    /**
     * イベントの処理を行うメソッド
     *
     * @param actionEvent 発生したイベント
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().startsWith("Choose")) {
            dialog.setVisible(true);
        } else if (actionEvent.getActionCommand().startsWith("Reset")) {
            model.setCurrentColor(Color.RED);
        } else if (actionEvent.getActionCommand().startsWith("Fill")) {
            model.setFill(true);
        } else if (actionEvent.getActionCommand().startsWith("Stroke")) {
            model.setFill(false);
        }
    }

    /**
     * JColorChooserからのイベントを処理するメソッド
     *
     * @param changeEvent 状態の変化を表すイベント
     */
    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        model.setCurrentColor(chooser.getColor());
    }
}
