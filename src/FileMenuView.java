import javax.swing.*;

/**
 * 新しい描画の開始、保存、ロード、エクスポートメニューを提供するView
 */
public class FileMenuView extends JMenu {
    /**
     * コンストラクタ
     *
     * @param m
     */
    public FileMenuView(DrawModel m) {
        super("File");
        FileMenuController controller = new FileMenuController(m, this);
        JMenuItem newImage = new JMenuItem("New Image");
        JMenuItem save = new JMenuItem("Save");
        JMenuItem load = new JMenuItem("Load");
        JMenuItem export = new JMenuItem("Export as");
        newImage.addActionListener(controller);
        save.addActionListener(controller);
        load.addActionListener(controller);
        export.addActionListener(controller);
        add(newImage);
        add(save);
        add(load);
        add(export);
    }
}
