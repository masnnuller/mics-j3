import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * FileMenuViewからのイベントを処理するコントローラ
 *
 */
public class FileMenuController implements ActionListener {
    private DrawModel model;
    private JFileChooser chooser;
    private FileMenuView view;

    /**
     * コンストラクタ
     *
     * @param m
     * @param v イベントを発生させたView
     */
    public FileMenuController(DrawModel m, FileMenuView v) {
        model = m;
        view = v;
        chooser = new JFileChooser();
    }

    /**
     * イベント処理
     *
     * @param actionEvent 発生したイベント
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().startsWith("New")) {
            model.clearFigure();
        } else if (actionEvent.getActionCommand().startsWith("Save") ||
                actionEvent.getActionCommand().startsWith("Load")) {
            File f = null;
            chooser.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (file.isDirectory())
                        return true;
                    if (file.getName().endsWith(".draw"))
                        return true;
                    return false;
                }

                @Override
                public String getDescription() {
                    return "Draw File(*.draw)";
                }
            });
            int result = chooser.showDialog(view.getRootPane(), null);
            if (result == JFileChooser.APPROVE_OPTION) {
                f = chooser.getSelectedFile();
                if (actionEvent.getActionCommand().startsWith("Save"))
                    model.saveObjects(f);
                else
                    model.loadObjects(f);
            }
        } else if (actionEvent.getActionCommand().startsWith("Export")) {
            File f = null;
            chooser.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (file.isDirectory())
                        return true;
                    if (file.getName().endsWith(".png"))
                        return true;
                    return false;
                }

                @Override
                public String getDescription() {
                    return "PNG(*.png)";
                }
            });
            int result = chooser.showDialog(view.getRootPane(), null);
            if (result == JFileChooser.APPROVE_OPTION) {
                f = chooser.getSelectedFile();
                model.exportImage(f, view.getRootPane());
            }
        }
    }
}