import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ShapeMenuViewからのイベントを処理するコントローラ
 */
public class ShapeMenuController implements ActionListener {
    private DrawModel model;

    /**
     * コンストラクタ
     *
     * @param m
     */
    public ShapeMenuController(DrawModel m) {
        model = m;
    }

    /**
     * イベント処理
     *
     * @param actionEvent 発生したイベント
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        System.out.println(actionEvent.getActionCommand());
        model.setCurrentFigure(actionEvent.getActionCommand());
    }
}
