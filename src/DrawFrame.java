import javax.swing.*;
import java.awt.*;

/**
 * メインフレームを提供するView、JFrameを継承している
 *
 * ここでModelのインスタンスを作成し他のViewに渡している
 */
public class DrawFrame extends JFrame {
    private DrawModel model;
    private ViewPanel view;
    private DrawController controller;
    private ConfigPanel config;

    /**
     * コンストラクタ
     * <p/>
     * フレームを作成、ConfigPanelとViewPanelを追加して表示している
     */
    public DrawFrame() {
        model = new DrawModel();
        controller = new DrawController(model);
        view = new ViewPanel(model, controller);
        config = new ConfigPanel(model);
        this.setBackground(Color.BLACK);
        this.setTitle("Draw Editor");
        this.setSize(500, 500);
        this.add(view);
        this.getRootPane().setJMenuBar(config);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
